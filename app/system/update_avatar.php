<?php
  session_start();
  if(empty($_SESSION['user_profile']) || empty($inside)){
    header('location:/');
  }
  require '../config.php';
  if(!empty($inside)){
    $user_profile = json_decode($_SESSION['user_profile']);
    $inside = true;
    //Mysql Declaração
    $mysqli = new MySQLi($mysql['host'], $mysql['user'], $mysql['password'], $mysql['database']);
    if (mysqli_connect_errno()){
        exit('{"error":"Ocorreu algum problema contate o administrador e passe o codigo 1252!"}');
    }
    $stmt = $mysqli->prepare("UPDATE dpft_perfis SET tentativas_login = ? WHERE email = ?");
    $stmt->bind_param('ss', $tentativas, $profile['email']);
    $stmt->execute(); 
    $stmt->close();
    exit('{"resultado":"trocado"}');
  }else{

  }
  
?>